import onionGpio
from led import Led


class App:
    iniciar = True
    led = None

    def __init__(self):
        print 'Manejador de LED se esta iniciando ...'
        self.__seleccionar_led()
        while self.iniciar:
            self.manejar_led()

    def manejar_led(self):
        switcher = {
            0: self.__cerrar,
            1: self.__encender_led,
            2: self.__apagar_led,
            3: self.__seleccionar_led
        }
        # Obtener la funcion guardada en el diccionario
        ejecutar = switcher.get(self.__seleccionar_opcion(), lambda: "Valor invalido")
        # Ejecutar la funcion
        ejecutar()

    def __cerrar(self):
        self.iniciar = False

    def __encender_led(self):
        self.led.encender()

    def __apagar_led(self):
        self.led.apagar()

    def __seleccionar_led(self):
        print('Pulse el numero del pin que desea controlar')
        pin = int(input())
        self.led = Led(pin)

    def __seleccionar_opcion(self):
        print('pulse 0 para cerrar\n' +
              '1 para encender el led\n' +
              '2 para apagar el led\n' +
              '3 para seleccionar otro pin')
        return int(input())


if __name__ == "__main__":
    App()
