import onionGpio


class Led:

    def __init__(self, pin):
        # instantiate a GPIO object
        self.gpio = onionGpio.OnionGpio(pin)

    def encender(self):
        self.gpio.setOutputDirection(1)
        self.gpio.setValue(1)

    def apagar(self):
        self.gpio.setOutputDirection(1)
        self.gpio.setValue(0)
